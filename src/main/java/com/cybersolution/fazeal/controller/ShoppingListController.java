package com.cybersolution.fazeal.controller;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.validation.Valid;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.MessageResponse;
import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.models.ShoppingListEntries;
import com.cybersolution.fazeal.models.ShoppingListRequest;
import com.cybersolution.fazeal.models.ShoppingListUpdateRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.ShoppingListEntriesRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.ShoppingListService;
import com.cybersolution.fazeal.service.impl.Messages;


/**
 * @author Faizan
 * Events controller for family shopping list
 *
 */

@CrossOrigin(origins = "*", maxAge = 4300)
@RestController
@RequestMapping("/api/v1/shoppingList")
public class ShoppingListController {
	
	private static final Logger LOGGER = LogManager.getLogger(ShoppingListController.class);
	
	@Autowired
	ShoppingListService shopListService;
	
	@Autowired
	Messages messages;
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	FamilyRespository familyRepo;
	
	@Autowired
	ShoppingListEntriesRepository shopItemsListRepo;
	
	/**
	 * @apiNote Used to get shopping list by id
	 * @Param Integer id
	 * @return ResponseEntity (Shopping List for Family)
	 *
	 */
	@GetMapping("/byId/{id}")
	public ResponseEntity getShoppingListById(@PathVariable(value = "id") Integer id) {
		LOGGER.traceEntry("getShoppingListById -> "+id);
		
		ShoppingList shopList = shopListService.findById(id);
		if(null == shopList) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_SHOP_LIST_ID_INVALID") +id));
		}
		LOGGER.info("getShoppingListById(): " + shopList);
		LOGGER.traceExit();
		return new ResponseEntity(shopList, HttpStatus.OK);
	}

	
	/**
	 * @apiNote Used to get shopping list by family id
	 * @Param Integer id
	 * @return ResponseEntity (Shopping List for Family)
	 *
	 */
	@GetMapping("/byFamilyId/{familyId}")
	public ResponseEntity getShoppingListByFamilyId(@PathVariable(value = "familyId") Integer id) {
		LOGGER.traceEntry("getShoppingListByFamilyId() -> "+id);
		
		ShoppingList shopList = shopListService.getShoppingListForFamilyOnId(id);
		if(null == shopList) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_SHOP_LIST__FAMILYID_INVALID") +id));
		}
		LOGGER.info("getShoppingListByFamilyId(): " + shopList);
		LOGGER.traceExit();
		return new ResponseEntity(shopList, HttpStatus.OK);
	}
	
	
	/**
	 * @apiNote Used to save shopping list by family id
	 * @Param ShoppingListRequest
	 * @return ResponseEntity (Shopping List for Family)
	 *
	 */
	@PostMapping("/create")
	public ResponseEntity<?> createShoppingList(@Valid @RequestBody ShoppingListRequest shopListRequest) {
		
		LOGGER.traceEntry("createShoppingList() --> " + shopListRequest);
		
		// @validation
		boolean userFamilyRelationExists = false;
		if (StringUtils.isEmpty(shopListRequest.getFamilyId())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_FAMILY_ID_MANDATORY")));
		}
		if (StringUtils.isEmpty(shopListRequest.getUserId())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NULL")));
		}
		
		if (StringUtils.isEmpty(shopListRequest.getItemMap())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_SHOP_LIST_WITHOUT_ITEM")));
		}
		
		if (StringUtils.isEmpty(shopListRequest.getShoppinglistName())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_SHOP_LIST_NAME")));
		}
		
		User user = userRepo.getById(shopListRequest.getUserId());
		if (null == user) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NOT_EXISTS")));
		}
		
		for(Family family: user.getFamilies()) {
			if(family.getFamilyId().equals(shopListRequest.getFamilyId())) {
				userFamilyRelationExists = true;
			}
		}
		
		if(!userFamilyRelationExists) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_FAMILY_USER_NOT_EXISTS")));
		}
		
		Set<ShoppingListEntries> shopListSet = new HashSet<>();
		for(String itemsName : shopListRequest.getItemMap().keySet()) {
		ShoppingListEntries shopListEntries = new ShoppingListEntries(itemsName, false, shopListRequest.getUserId(), user.getUsername());
		shopListSet.add(shopListEntries);
		shopItemsListRepo.save(shopListEntries);
		}
	
		ShoppingList shoppingList = new ShoppingList(shopListRequest.getShoppinglistName(),shopListRequest.getFamilyId() , getCurrentDateTime(), true,null);
		shoppingList.setShopping_items_list(shopListSet);
		shopListService.save(shoppingList);
		
		
		LOGGER.info("createShoppingList(): shopping list saved: " + shoppingList);
		LOGGER.traceExit();
		return new ResponseEntity(shoppingList, HttpStatus.OK);
	}
	
	
	/**
	 * @apiNote Used to update shopping list by family id only status and shoplist name
	 * @Param ShoppingListRequest
	 * @return ResponseEntity (Shopping List for Family)
	 *
	 */
	@PostMapping("/update/byId")
	public ResponseEntity<?> updateShoppingList (@Valid @RequestBody ShoppingListUpdateRequest shopListRequest) {
		
		LOGGER.traceEntry("updateShoppingList() --> " + shopListRequest);
		
		if (StringUtils.isEmpty(shopListRequest.getId())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_SHOP_LIST_ID_INVALID")));
		}
		
		if (StringUtils.isEmpty(shopListRequest.getUserId())) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NULL")));
		}
		ShoppingList shopList = shopListService.findById(shopListRequest.getId());
		
		if(null == shopList) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_SHOP_LIST_ID_INVALID")));
		}
		
		User user = userRepo.getById(shopListRequest.getUserId());
		if (null == user) {
			return ResponseEntity.badRequest().body(new MessageResponse(messages.get("ERROR_USER_NOT_EXISTS")));
		}
		
		Set<ShoppingListEntries> shopListSet = new HashSet<>();
		if(null != shopListRequest.getItemMap()) {
			Set<ShoppingListEntries> shopListEntries = shopItemsListRepo.getShoppingListEntriesByListId(shopListRequest.getId());
			shopItemsListRepo.deleteAll(shopListEntries);
			for(String itemsName : shopListRequest.getItemMap().keySet()) {
			ShoppingListEntries shopListEntry = new ShoppingListEntries(itemsName, shopListRequest.getItemMap().get(itemsName), shopListRequest.getUserId(), user.getUsername());
			shopListSet.add(shopListEntry);
			shopItemsListRepo.save(shopListEntry);
			}
		}
		
		shopList.set_active(shopListRequest.isListStatus());
		shopList.setShopping_items_list(shopListSet);
		shopList.setShoppinglistName(shopListRequest.getShoppingListName());
		shopList.setUpdated_at(getCurrentDateTime());
		shopListService.save(shopList);
		LOGGER.info("updateShoppingList(): " + shopList);
		LOGGER.traceExit();
		return new ResponseEntity(shopList, HttpStatus.OK);
	}

	
	/**
	 * @apiNote Used to delete items from shopping list
	 * @Param Integer id (itemId)
	 * 
	 * @return Map (deleted,true)
	 *
	 */
	@DeleteMapping("/deleteItems/{id}")
	public Map<String, Boolean> deleteShoppingListItems(@PathVariable(value = "id") Integer itemId) {
		LOGGER.traceEntry("deleteShoppingListItems -> "+itemId);
		
		ShoppingListEntries shopListItems = shopItemsListRepo.getById(itemId);
		shopItemsListRepo.delete(shopListItems);
		
		Map<String, Boolean> response = new HashMap<>();
		LOGGER.info("deleteShoppingListItems()");
		response.put("deleted", Boolean.TRUE);
		LOGGER.traceExit();
		return response;
	}
	
	/**
	 * @apiNote Used to delete items from shopping list
	 * @Param Integer id (itemId)
	 * 
	 * @return Map (deleted,true)
	 *
	 */
	@DeleteMapping("/deleteList/{id}")
	public Map<String, Boolean> deleteShoppingList(@PathVariable(value = "id") Integer shoppingListId) {
		LOGGER.traceEntry("deleteShoppingList -> "+shoppingListId);
		
		ShoppingList shopList = shopListService.findById(shoppingListId);
		shopListService.delete(shopList);
		
		Map<String, Boolean> response = new HashMap<>();
		LOGGER.info("deleteShoppingList()");
		response.put("deleted", Boolean.TRUE);
		LOGGER.traceExit();
		return response;
	}
	
	private String getCurrentDateTime() {
		SimpleDateFormat formatter  = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date=  new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
}









