package com.cybersolution.fazeal.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "event_records")
public class EventsRelation {
	
	@Id
	private Integer  eid;
	private String uid;
	private String uemail;
	private String username;
	private String  family_name;
	private String  event;
	private String  edate;
	private String  estart;
	private String  eend;
	private String  is_shared;
	private String  family_id;

}
