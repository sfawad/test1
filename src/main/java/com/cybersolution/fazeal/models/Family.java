package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Table(name = "family")
public class Family {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer familyId;
	@NotBlank
	private String familyName;
	private String info;
	private Long phone;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String country;
	
	/*
	 * @JsonIgnore
	 * 
	 * @OneToMany(mappedBy = "family", cascade = CascadeType.ALL) private Set<User>
	 * users = new HashSet<>();
	 */
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_pending_family", 
				joinColumns = @JoinColumn(name = "family_id"), 
				inverseJoinColumns = @JoinColumn(name = "user_id"))
	private Set<User> pendingUser = new HashSet<>();

	public Family(@NotBlank String familyName, String info, Long phone, String address1,
			String address2, String city, String state, String zip, String country) {
		super();
		this.familyName = familyName;
		this.info = info;
		this.phone = phone;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.country = country;
	}
	
		public Family() {}
}
