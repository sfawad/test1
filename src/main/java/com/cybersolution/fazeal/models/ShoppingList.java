package com.cybersolution.fazeal.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "shopping")
public class ShoppingList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String shoppinglistName;
	
	private Integer familyId;
	
	private String date_create;
	
	private boolean is_active;

	private String updated_at;
	
	@OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
	@JoinColumn(name="shoppinglist_id")
	private Set<ShoppingListEntries> shopping_items_list = new HashSet<>();

	public ShoppingList(String shoppinglistName, Integer familyId, String date_create, boolean is_active,
			String updated_at) {
		super();
		this.shoppinglistName = shoppinglistName;
		this.familyId = familyId;
		this.date_create = date_create;
		this.is_active = is_active;
		this.updated_at = updated_at;
	}

}
