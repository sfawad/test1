package com.cybersolution.fazeal.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "shoppinglist_items")
public class ShoppingListEntries {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private String item;
	
	private boolean item_status;
	
	private Integer userId;
	
	private String userName;
	
	private Integer shoppinglist_id;
	
	public ShoppingListEntries(String item, boolean item_status, Integer userId, String userName) {
		super();
		this.item = item;
		this.item_status = item_status;
		this.userId = userId;
		this.userName = userName;
	}
}
