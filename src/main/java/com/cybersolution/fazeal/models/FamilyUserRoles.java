package com.cybersolution.fazeal.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Table(name = "family_users_roles")
public class FamilyUserRoles {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	private Integer familyId;
	
	private Integer userId;
	
	private String role;
	
	// Relation with family
	private String relation;

	public FamilyUserRoles() {}
	
	public FamilyUserRoles(Integer familyId, Integer userId, String role, String relation) {
		super();
		this.familyId = familyId;
		this.userId = userId;
		this.role = role;
		this.relation = relation;
	}
}
