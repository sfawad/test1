package com.cybersolution.fazeal;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FazealSpringbootMicroserviceApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(FazealSpringbootMicroserviceApplication.class, args);
	}
	

}
