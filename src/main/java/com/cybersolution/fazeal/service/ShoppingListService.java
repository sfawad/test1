package com.cybersolution.fazeal.service;

import com.cybersolution.fazeal.models.ShoppingList;

public interface ShoppingListService {
	
	ShoppingList findById(Integer id);

	ShoppingList save(ShoppingList shopList);
	
	ShoppingList getShoppingListForFamilyOnId(Integer familyId);
	
	ShoppingList delete(ShoppingList shopList);
	
}
