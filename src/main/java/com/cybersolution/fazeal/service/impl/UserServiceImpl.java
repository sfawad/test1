package com.cybersolution.fazeal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	UserRepository repo;

	@Override
	public User getById(Integer id) {
		return repo.findById(id).get();
	}

}
