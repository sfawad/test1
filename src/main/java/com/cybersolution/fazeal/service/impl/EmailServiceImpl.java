package com.cybersolution.fazeal.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.service.EmailService;

@Service
public class EmailServiceImpl implements EmailService{
	
	@Autowired
	public JavaMailSender mailSender;
	
	@Override
	public void sendEmail(SimpleMailMessage email) {
		mailSender.send(email);
		
	}

}
