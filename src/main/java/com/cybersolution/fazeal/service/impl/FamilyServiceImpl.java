package com.cybersolution.fazeal.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.service.FamilyService;

@Service
public class FamilyServiceImpl implements FamilyService {
	
	@Autowired
	private FamilyRespository familyRespository;

	@Override
	public Family findById(Integer id) {
		// TODO Auto-generated method stub
		return familyRespository.getById(id);
	}

	@Override
	public Family getFamilyInfoFromName(String name) {
		return familyRespository.getFamilyInfoFromName(name);
	}

	@Override
	public Family getById(Integer familyId) {
		return  familyRespository.getById(familyId);
	}

	@Override
	public Family save(Family family) {
		return familyRespository.save( family);
	}

	@Override
	public Family saveFamilyInfo(String familyName, String info, Long phone, String address1,
			String address2, String city, String state, String zip, String country) {
		  Family family = new Family(familyName, info, phone, address1, address2,
				  city, state, zip, country); 
		  familyRespository.save(family);
		 return family;
	}


}
