package com.cybersolution.fazeal.service;

import com.cybersolution.fazeal.models.Family;

public interface FamilyService {

	Family findById(Integer id);
	// service

	Family getFamilyInfoFromName(String name);

	Family getById(Integer familyId);

	Family save(Family family);

	Family saveFamilyInfo(String familyName, String info, Long phone, String address1, String address2,
			String city, String state, String zip, String country);
	
}
