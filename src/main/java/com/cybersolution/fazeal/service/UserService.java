package com.cybersolution.fazeal.service;

import com.cybersolution.fazeal.models.User;

public interface UserService {
	User getById(Integer id);
}
