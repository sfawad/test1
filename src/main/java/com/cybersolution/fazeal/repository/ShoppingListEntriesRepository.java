package com.cybersolution.fazeal.repository;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.ShoppingListEntries;


@Repository
public interface ShoppingListEntriesRepository extends JpaRepository<ShoppingListEntries, Integer> {
	
	@Query(value = "Select c from ShoppingListEntries c where c.shoppinglist_id = ?1")
	Set<ShoppingListEntries> getShoppingListEntriesByListId(Integer shoppingListId);
	
}
