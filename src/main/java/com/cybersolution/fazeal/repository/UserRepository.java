package com.cybersolution.fazeal.repository;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.User;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	Optional<User> findByUsername(String username);

	Boolean existsByUsername(String username);

	Boolean existsByEmail(String email);

	@Query(value = "Select * from users where id in (select user_id from user_family where family_id = :familyId) and id <> :userId", nativeQuery = true)
	List<User> getUsersListForAFamily(@Param("familyId") Integer familyId, @Param("userId") Integer userId);
	
	
	@Query(value= "Select * from users where email= :email", nativeQuery = true)
	User getUserFromEmail(String email);
	
	@Query(value= "Select * from users where reset_token= :token", nativeQuery = true)
	User getUserFromToken(String token);
}
