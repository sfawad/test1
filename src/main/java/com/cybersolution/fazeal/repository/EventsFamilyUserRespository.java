package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.EventsFamilyUser;

@Repository 
public interface EventsFamilyUserRespository extends JpaRepository<EventsFamilyUser, Integer>{

	 @Query(value = "select * from events_user_family where event_id = ?1", nativeQuery= true)
	 List<EventsFamilyUser> findEventsByEventId(Integer eventId);
	 
	 @Query(value = "select * from events_user_family where family_id = ?1", nativeQuery= true)
	 List<EventsFamilyUser> findEventsByFamilyId(Integer familyId);
}
