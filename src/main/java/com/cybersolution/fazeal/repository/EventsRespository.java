package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.Events;

@Repository 
public interface EventsRespository extends JpaRepository<Events, Integer>{
	
	@Query("Select c from Events c where c.userId = ?1 ")
	public List<Events> findByUserId(Integer userId);

}
