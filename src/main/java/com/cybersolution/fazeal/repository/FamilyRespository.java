package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.Family;

@Repository 
public interface FamilyRespository extends JpaRepository<Family, Integer>{
	
	@Query(value = "Select c from Family c where c.familyName = ?1")
	public Family getFamilyInfoFromName(String familyName);
}
