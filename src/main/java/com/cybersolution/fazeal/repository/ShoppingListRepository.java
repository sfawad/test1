package com.cybersolution.fazeal.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.ShoppingList;


@Repository
public interface ShoppingListRepository extends JpaRepository<ShoppingList, Integer> {
	
	@Query(value = "Select c from ShoppingList c where c.familyId = ?1")
	ShoppingList getShoppingListForFamilyOnId(Integer familyId);
	
}
