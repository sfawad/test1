package com.cybersolution.fazeal.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.cybersolution.fazeal.models.EventsRelation;

@Repository 
public interface EventsRelationRespository extends JpaRepository<EventsRelation, Integer>{
	
	 @Query(value = "select * from event_records where family_id = :familyId and is_shared = 1", nativeQuery= true)
	 List<EventsRelation> findSharedEventsForFamily(@Param("familyId") Integer familyId);
	 
	 @Query(value = "select * from event_records where uid = :uid and is_shared = 0", nativeQuery= true)
	 List<EventsRelation> findPrivateEventsForUser(@Param("uid") Integer uid);

}
