package com.cybersolution.fazeal.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.cybersolution.fazeal.models.FamilyUserRoles;

@Repository 
public interface FamilyUserRoleRespository extends JpaRepository<FamilyUserRoles, Integer>{
	
	 @Query(value = "select * from family_users_roles where family_id = :familyId and user_id = :userId", nativeQuery= true)
	 FamilyUserRoles findRoleForUserInFamily(@Param("familyId") Integer familyId, @Param("userId") Integer userId);
	 
	 @Query(value = "select * from family_users_roles where family_id = :familyId", nativeQuery= true)
	 List<FamilyUserRoles> findAllUserRolesInFamily(@Param("familyId") Integer familyId);
}
