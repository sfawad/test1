package com.cybersolution.fazeal.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.controller.FamilyController;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.FamilyUserRoleRespository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.FamilyService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class FamilyControllerTest {
	@InjectMocks
	private FamilyController familyController;


	@Mock
	FamilyService famService;
	
	@Mock
	UserRepository userRepo;
	
	@Mock
	RoleRepository roleRepo;
	
	@Mock
	FamilyUserRoleRespository famUserRoleRepo;

	@Test
	public void saveFamilyTestCase() throws Exception {
		
		User user = new User("user1","email1.cc.com","Ew22232wwwWESD",true);
		Optional<User> userOp = Optional.of(user);
		Role roles = new Role(null, ERole.ROLE_ADMIN);
		Optional<Role> roleOp = Optional.of(roles);
		
		Family family  = createObj();
		when(famService.getFamilyInfoFromName("")).thenReturn(null);
		when(userRepo.findById(new Integer(1))).thenReturn(userOp);
		when(roleRepo.findByName(ERole.ROLE_ADMIN)).thenReturn(roleOp);
		ResponseEntity<?> result = familyController.saveFamilyInfo(Integer.valueOf(1),"TEST Family","INFO",Long.valueOf("1234567890"),"Addr1","Addr2","City","Sind","74600","Country","Father");
		Object response = result.getBody();
		assertEquals(family, response);
	}
	
	@Test
	public void updateFamilyTestCase() throws Exception {
		
		Family family  = createObj();
		when(famService.getById(1)).thenReturn(family);
		ResponseEntity result = familyController.updateFamilyInfo(Integer.valueOf(1),"INFO Update",Long.valueOf("1234567890"),"Addr1","Addr2","City","Sind","74600","Country");
		Object response = result.getBody();
		family.setInfo("INFO Update");
		assertEquals(family, response);
	}
	
	@Test
	public void getFamilyByIdTestCase() throws Exception {
		
		Family family  = createObj();
		when(famService.findById(1)).thenReturn(family);
		ResponseEntity<?> result = familyController.getFamilyById(Integer.valueOf(1));
		Family response = (Family) result.getBody();
		assertEquals(family, response);
	}
	
	private Family createObj() {
		Family family  = new Family("TEST Family","INFO",Long.valueOf("1234567890"),"Addr1","Addr2","City","Sind","74600","Country");
		return family;
	}

}
