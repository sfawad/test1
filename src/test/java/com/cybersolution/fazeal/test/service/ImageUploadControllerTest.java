package com.cybersolution.fazeal.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.cybersolution.fazeal.controller.ImageUploadController;
import com.cybersolution.fazeal.service.ImageService;
import com.cybersolution.fazeal.service.UserService;
import com.cybersolution.fazeal.service.impl.Dropbox;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ImageUploadControllerTest {
	@InjectMocks
	private ImageUploadController imageController;

	@Mock
	private ImageService imageService;

	@Mock
	private UserService userService;

	@Mock
	private Dropbox dropbox;

	@Mock
	public RestTemplate restTemplate;

	@Test
	public void deleteImageSucessTestCase() throws Exception {
		Long imageId = 3L;
		boolean expected = true;

		when(imageService.delete(imageId)).thenReturn(true);

		ResponseEntity<Boolean> delete = imageController.delete(imageId);
		assertEquals(expected, delete.getBody());
	}
	@Test
	public void deleteImageFailTestCase() throws Exception {
		Long imageId = 3L;
		boolean expected = false;

		when(imageService.delete(imageId)).thenReturn(false);

		ResponseEntity<Boolean> delete = imageController.delete(imageId);
		assertEquals(expected, delete.getBody());
	}

}
