package com.cybersolution.fazeal.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.cybersolution.fazeal.controller.AuthController;
import com.cybersolution.fazeal.models.ERole;
import com.cybersolution.fazeal.models.Role;
import com.cybersolution.fazeal.models.SignupRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.FamilyRespository;
import com.cybersolution.fazeal.repository.ImageRepository;
import com.cybersolution.fazeal.repository.RoleRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.EmailService;
import com.cybersolution.fazeal.service.impl.Messages;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AuthControllerTest {
	@InjectMocks
	private AuthController authController;

	@Mock
	UserRepository userRepository;

	@Mock
	PasswordEncoder encoder;


	@Mock
	ImageRepository imageRepo;
	
	@Mock
	FamilyRespository familyRepo;
	
	@Mock
	RoleRepository roleRepo;
		
	@Mock
	Messages messages;
	
	@Mock
	EmailService emailService;
	
	@Mock
	Environment environment;
	
    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
    }

	@Test
	public void registerUserAccepted() throws Exception {
		String expected = "MessageResponse(message=User registered successfully!)";
		
		SignupRequest signupRequest = new SignupRequest("testUser", "test@test.com", "", "123A2!@kjdsllkd", false,null,null,null);
		Role roles = new Role(null, ERole.ROLE_USER);
		Optional<Role> roleOp = Optional.of(roles);
		when(encoder.encode("123A2!@kjdsllkd")).thenReturn("testEncrypted");
		when(userRepository.existsByUsername(signupRequest.getUsername())).thenReturn(false);
		when(familyRepo.getFamilyInfoFromName("testFamily")).thenReturn(null);
		when(roleRepo.findByName(ERole.ROLE_USER)).thenReturn(roleOp);
		ResponseEntity<?> actual = authController.registerUser(signupRequest);

		Object result = actual.getBody();
		assertEquals(expected, result.toString());
	}

	@Test
	public void registerUserDuplicatedEmail() throws Exception {
		String expected = "MessageResponse(message=Error: Username is already taken!)";
		SignupRequest signupRequest = new SignupRequest("testUser", "test@test.com", "", "123A2!@kjdsllkd", false,null,null,null);

		when(userRepository.existsByUsername(signupRequest.getUsername())).thenReturn(Boolean.TRUE);
		when(userRepository.existsByEmail(signupRequest.getEmail())).thenReturn(Boolean.FALSE);
		when(messages.get("ERROR_USER_NAME")).thenReturn("Error: Username is already taken!");
		ResponseEntity<?> actual = authController.registerUser(signupRequest);

		Object result = actual.getBody();
		assertEquals(expected, result.toString());
	}

	@Test
	public void deleteUserSucessTestCase() throws Exception {
		Set<Role> userRoles = new HashSet<Role>();
		userRoles.add(new Role(null, ERole.ROLE_USER));
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		Optional<User> op = Optional.of(user);
		Map<String, Boolean> expected = new HashMap<String, Boolean>();
		expected.put("deleted", Boolean.TRUE);
		Integer userId = 2;

		when(imageRepo.setUserIdNull(userId)).thenReturn(1);
		when(userRepository.findById(userId)).thenReturn(op);

		Map<String, Boolean> result = authController.deleteUser(userId);
		assertEquals(expected, result);
	}

	@Test
	public void enableUserSucessTestCase() throws Exception {
		Set<Role> userRoles = new HashSet<Role>();
		userRoles.add(new Role(null, ERole.ROLE_USER));
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		Optional<User> op = Optional.of(user);
		boolean expected = true;
		Integer userId = 2;
		boolean enabled = true;
		
			
		when(userRepository.findById(userId)).thenReturn(op);
		when(userRepository.save(user)).thenReturn(user);

		ResponseEntity<User> result = authController.updateUserEnabled(userId, enabled,userId);
		user.setEnabled(enabled);
		assertEquals(expected, result.getBody().isEnabled());
	}

	@Test
	public void updateUserDataSucessTestCase() throws Exception {
		Set<Role> userRoles = new HashSet<Role>();
		userRoles.add(new Role(null, ERole.ROLE_USER));
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		Optional<User> op = Optional.of(user);
		User expected = user;
		Integer userId = 2;
		String newPassword = "123123123";
		String newEmail = "update@email.com";
		String newUserName = "newUser";
		
		when(encoder.encode(newPassword)).thenReturn("testEncrypted");
		when(userRepository.findById(userId)).thenReturn(op);
		when(userRepository.save(user)).thenReturn(user);

		ResponseEntity<User> result = authController.updateUser(userId, newUserName, newEmail, newPassword);
		user.setEmail(newEmail);
		user.setPassword(newPassword);
		user.setUsername(newUserName);
		assertEquals(expected, result.getBody());
	}
	
//	@Test
//	public void forgotPasswordTestCase() throws Exception {
//		User user = new User("testUser", "test1@test.com", "Encrepted", false);
//		String expected = "MessageResponse(message=Email sent to user with reset password link)";
//		when(userRepository.getUserFromEmail("test@test.com")).thenReturn(user);
//		when(messages.get("PASSWORD_EMAIL_SENT_SUCCES")).thenReturn("Email sent to user with reset password link");
//		ResponseEntity<?> result = authController.forgotPassword("test1@test.com");
//		System.out.println(result.getBody());
//		assertEquals(expected, result.getBody().toString());
//	}
	
	
	@Test
	public void verifyResetTokenTestCase() throws Exception {
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		Map expected = new HashMap<>();
		expected.put("isValid", true);
		when(userRepository.getUserFromToken("token")).thenReturn(user);
		Map result = authController.verifyResetToken("token");
		assertEquals(expected, result);
	}
	
	@Test
	public void resetPasswordTestCase() throws Exception {
		String expected = "MessageResponse(message=Password has been updated succesfully)";
		User user = new User("testUser", "test@test.com", "Encrepted", false);
		when(environment.getProperty("token-expiry")).thenReturn("64000");
		when(messages.get("PASSWORD_UPDATED_SUCCESS")).thenReturn("Password has been updated succesfully");
		when(encoder.encode("")).thenReturn("testEncrypted");
		when(userRepository.getUserFromToken("token")).thenReturn(user);
		ResponseEntity<?> result = authController.resetPassword("token", "passw0rd");
		assertEquals(expected, result.getBody().toString());
	}
}
