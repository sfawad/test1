package com.cybersolution.fazeal.test.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import com.cybersolution.fazeal.controller.ShoppingListController;
import com.cybersolution.fazeal.models.Family;
import com.cybersolution.fazeal.models.ShoppingList;
import com.cybersolution.fazeal.models.ShoppingListEntries;
import com.cybersolution.fazeal.models.ShoppingListRequest;
import com.cybersolution.fazeal.models.ShoppingListUpdateRequest;
import com.cybersolution.fazeal.models.User;
import com.cybersolution.fazeal.repository.ShoppingListEntriesRepository;
import com.cybersolution.fazeal.repository.UserRepository;
import com.cybersolution.fazeal.service.ShoppingListService;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ShoppingListControllerTest {
	@InjectMocks
	private ShoppingListController shopListController;


	@Mock
	ShoppingListService shopListService;
	
	@Mock
	UserRepository userRepo;
	
	@Mock
	ShoppingListEntriesRepository shopItemsListRepo;
	
	@Test
	public void getShoppingListByIdTestCase() throws Exception {
		
		ShoppingList shopList  = createObj();
		when(shopListService.findById(1)).thenReturn(shopList);
		ResponseEntity<?> result = shopListController.getShoppingListById(Integer.valueOf(1));
		Object response = result.getBody();
		assertEquals(shopList, response);
	}
	
	@Test
	public void getShoppingListByFamilyIdTestCase() throws Exception {
		
		ShoppingList shopList  = createObj();
		when(shopListService.getShoppingListForFamilyOnId(1)).thenReturn(shopList);
		ResponseEntity<?> result = shopListController.getShoppingListByFamilyId(Integer.valueOf(1));
		Object response = result.getBody();
		assertEquals(shopList, response);
	}
	
	@Test
	public void createShoppingListTestCase() throws Exception {
		
		User user = new User("user1","email1.cc.com","Ew22232wwwWESD",true);
		Set<Family> famSet = new HashSet<>();
		Family family  = new Family("TEST Family","INFO",Long.valueOf("1234567890"),"Addr1","Addr2","City","Sind","74600","Country");
		family.setFamilyId(1);
		famSet.add(family);
		user.setFamilies(famSet);
		ShoppingListRequest shopList  = createPOJOObj();
		ShoppingList shopResultList = createObj();
		when(userRepo.getById(1)).thenReturn(user);
		ResponseEntity<?> result = shopListController.createShoppingList(shopList);
		Object response = result.getBody();
		assertEquals(shopResultList, response);
	}
	
	@Test
	public void updateShoppingListTestCase() throws Exception {
		
		User user = new User("user1","email1.cc.com","Ew22232wwwWESD",true);
		ShoppingListUpdateRequest shopList  = createUpdatePOJOObj();
		Set<Family> famSet = new HashSet<>();
		Family family  = new Family("TEST Family","INFO",Long.valueOf("1234567890"),"Addr1","Addr2","City","Sind","74600","Country");
		family.setFamilyId(1);
		famSet.add(family);
		user.setFamilies(famSet);
		ShoppingList shopResultList = createObj();
		shopResultList.getShopping_items_list().add(new ShoppingListEntries("three", true, 1, "user1"));
		when(userRepo.getById(1)).thenReturn(user);
		when(shopListService.findById(1)).thenReturn(shopResultList);
		ResponseEntity<?> result = shopListController.updateShoppingList(shopList);
		Object response = result.getBody();
		assertEquals(shopResultList, response);
	}
	
	@Test
	public void deleteShoppingListTestCase() throws Exception{
		ShoppingList shopList = createObj();
		Map<String, Boolean> resultMap = new HashMap<String, Boolean>();
		resultMap.put("deleted", Boolean.TRUE);
		when(shopListService.findById(1)).thenReturn(shopList);
		Map<String, Boolean> mp1 = shopListController.deleteShoppingList(1);
		assertEquals(resultMap, mp1);
	}
	
	@Test
	public void deleteShoppingListItemsTestCase() throws Exception{
		ShoppingListEntries shopListEntry = new ShoppingListEntries("one", false, 1, "user1");
		Map<String, Boolean> resultMap = new HashMap<String, Boolean>();
		resultMap.put("deleted", Boolean.TRUE);
		when(shopItemsListRepo.getById(1)).thenReturn(shopListEntry);
		Map<String, Boolean> mp1 = shopListController.deleteShoppingListItems(1);
		assertEquals(resultMap, mp1);
	}
	
	private ShoppingList createObj() {
		ShoppingList shopList  = new ShoppingList("TEST Shop List", 1, getCurrentDateTime(), true, null);
		Set<ShoppingListEntries> shopListEntries = new HashSet<>();
		ShoppingListEntries shopListEntry = new ShoppingListEntries("one", false, 1, "user1");
		shopListEntries.add(shopListEntry);
		shopListEntry = new ShoppingListEntries("two", false, 1, "user1");
		shopListEntries.add(shopListEntry);
		shopList.setShopping_items_list(shopListEntries);
		return shopList;
	}
	
	private ShoppingListRequest createPOJOObj() {
		ShoppingListRequest shopList  = new ShoppingListRequest();
		
		Map<String,Boolean> strMap = new HashMap<>();
		strMap.put("one", false);
		strMap.put("two", false);
		
		shopList.setFamilyId(1);
		shopList.setUserId(1);
		shopList.setItemMap(strMap);
		shopList.setShoppinglistName("TEST Shop List");
		return shopList;
	}
	
	private ShoppingListUpdateRequest createUpdatePOJOObj() {
		ShoppingListUpdateRequest shopList  = new ShoppingListUpdateRequest();
		
		Map<String,Boolean> strMap = new HashMap<>();
		strMap.put("one", false);
		strMap.put("two", false);
		strMap.put("three", true);
		
		shopList.setId(1);
		shopList.setUserId(1);
		shopList.setItemMap(strMap);
		shopList.setShoppingListName("TEST Shop List");
		shopList.setListStatus(true);
		return shopList;
	}

	private String getCurrentDateTime() {
		SimpleDateFormat formatter  = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		Date date=  new Date(System.currentTimeMillis());
		return formatter.format(date);
	}
}
